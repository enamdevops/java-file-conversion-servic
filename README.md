# Online File Conversion Tools

* [Java JDK_11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html/) install jdk_11 version
* [Maven](https://maven.apache.org/download.cgi)

### Getting Started - Build War File

```shell
$ mvn spring-boot:run
$ mvn clean install
```

## Framework Structure:
##Requirements:
- **_Web Server:_** Apache Tomcat
- **_Database:_** MySql
