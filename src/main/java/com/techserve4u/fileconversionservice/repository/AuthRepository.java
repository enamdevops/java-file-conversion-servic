package com.techserve4u.fileconversionservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.techserve4u.fileconversionservice.entity.Auth;

@Transactional
@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {
	
	public Auth findByEmail(String email);
	
	// @Query(value = "select * from users where email = ?1", nativeQuery = true)
	// public String findByEmail(String email);
	
	@Query(value = "select password from users where email = ?1", nativeQuery = true)
	public String findByEmailPassword(String email);
	
	@Modifying
    @Query(value = "Update users SET otp=:otp WHERE email=:email", nativeQuery = true)
    public void updateOtp(@Param("otp") String otp, @Param("email") String email);
	
	@Modifying
    @Query(value = "Update users SET password=:password WHERE otp=:otp And email=:email", nativeQuery = true)
    public void updatePasswordByOtp(@Param("password") String password, @Param("otp") String otp, @Param("email") String email);
}
