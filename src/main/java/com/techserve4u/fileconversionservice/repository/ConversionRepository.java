package com.techserve4u.fileconversionservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techserve4u.fileconversionservice.entity.Conversion;

@Repository
public interface ConversionRepository extends JpaRepository<Conversion, Long> {
	
}
