package com.techserve4u.fileconversionservice.restcontroller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techserve4u.fileconversionservice.entity.Auth;
import com.techserve4u.fileconversionservice.repository.AuthRepository;
import com.techserve4u.fileconversionservice.service.AuthService;
import com.techserve4u.fileconversionservice.service.MailSenderService;

@RestController
@RequestMapping("/api/v1")
public class AuthController {
	
	@Autowired
	AuthService authService;
	
	@Autowired
	AuthRepository authRepository;
	
	@Autowired
	MailSenderService mailSenderService;
	
	
	@PostMapping("/signup")
    public ResponseEntity<Auth> signup(@RequestBody Auth auth){
		
		Auth checkUserEmail = authRepository.findByEmail(auth.getEmail());

		if (checkUserEmail != null)
			return new ResponseEntity("User Already Exists", HttpStatus.CONFLICT);
		
		if (auth.getPassword().length() < 8)
			return new ResponseEntity("Password must be at least 8 Characters", HttpStatus.UNPROCESSABLE_ENTITY);

		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		if (auth.getPassword().matches(pattern) == false)
			return new ResponseEntity("Complex Password Required", HttpStatus.UNPROCESSABLE_ENTITY);
		
		Auth signupOne = authService.signup(auth);
        return new ResponseEntity<Auth>(signupOne, HttpStatus.CREATED);
    }
	
	
	@PostMapping("/signin")
    public ResponseEntity<JsonNode> signin(@RequestBody Auth auth){
		
		String pass = authRepository.findByEmailPassword(auth.getEmail());
		
		if ((pass != null) && pass.equals(auth.getPassword())) {
			try {
				ObjectMapper mapper = new ObjectMapper();
		        JsonNode json = mapper.readTree("{\"Status\": 1, \"Message\": \"Login Success\"}");
		        return ResponseEntity.ok(json);
			} catch (Exception e) {
			}
		} else {
			try {
				ObjectMapper mapper = new ObjectMapper();
		        JsonNode json = mapper.readTree("{\"Status\": 0, \"Message\": \"Login Failure\"}");
		        return ResponseEntity.ok(json);
			} catch (Exception e) {
			}
		}
		
        return new ResponseEntity<JsonNode>(HttpStatus.OK);
    }
	
	@PostMapping("/forgot-password")
    public ResponseEntity<JsonNode> forgotPassword(@RequestBody Auth auth){
		
		if (authRepository.findByEmail(auth.getEmail()) != null) {
			
			// make OTP
			Random rand = new Random();
			String otpCode = Integer.toString(rand.nextInt(999999));

			// update OTP to DB
			authRepository.updateOtp(otpCode, auth.getEmail());

			// sent OTP to Email
			mailSenderService.sendPasswordOtpMail(auth.getEmail(), otpCode);
	        
			try {
				ObjectMapper mapper = new ObjectMapper();
		        JsonNode json = mapper.readTree("{\"Status\": 1, \"Message\": \"OTP Sent\"}");
		        return ResponseEntity.ok(json);
			} catch (Exception e) {
			}
			
			return new ResponseEntity<JsonNode>(HttpStatus.OK);
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();
	        JsonNode json = mapper.readTree("{\"Status\": 0, \"Message\": \"Email not Found\"}");
	        return ResponseEntity.ok(json);
		} catch (Exception e) {
		}
		
		return new ResponseEntity<JsonNode>(HttpStatus.UNAUTHORIZED);
    }
	
	@PostMapping("/reset-password")
    public ResponseEntity<JsonNode> resetPassword(@RequestBody Auth auth){
		
		String inputEmail = auth.getEmail();
		String inputOtp = auth.getOtp();
		String inputPassword = auth.getPassword();
		
		String dbEmail = authRepository.findByEmail(inputEmail).getEmail();
		String dbOtp = authRepository.findByEmail(inputEmail).getOtp();
		
		if (inputOtp.equals(dbOtp) && inputEmail.equalsIgnoreCase(dbEmail)) {
			authRepository.updatePasswordByOtp(inputPassword, inputOtp, inputEmail);
			authRepository.updateOtp(null, inputEmail);

			try {
				ObjectMapper mapper = new ObjectMapper();
		        JsonNode json = mapper.readTree("{\"Status\": 1, \"Message\": \"Password Changed Successfully\"}");
		        return ResponseEntity.ok(json);
			} catch (Exception e) {
			}
			
			return new ResponseEntity<JsonNode>(HttpStatus.OK);
		}
		
		try {
			ObjectMapper mapper = new ObjectMapper();
	        JsonNode json = mapper.readTree("{\"Status\": 0, \"Message\": \"Wrong OTP Provided\"}");
	        return ResponseEntity.ok(json);
		} catch (Exception e) {
		}
		
        return new ResponseEntity<JsonNode>(HttpStatus.UNAUTHORIZED);
    }
}
