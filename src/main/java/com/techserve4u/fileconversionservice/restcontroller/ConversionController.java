package com.techserve4u.fileconversionservice.restcontroller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.techserve4u.fileconversionservice.entity.Conversion;
import com.techserve4u.fileconversionservice.service.ConversionService;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;

@RestController
@RequestMapping("/api/v1")
public class ConversionController {
	
	@Autowired
	ConversionService conversionService;
	
	@PostMapping("/word-to-pdf")
    public ResponseEntity<Conversion> save(@RequestBody Conversion conversion){

		try {
			InputStream in = new URL(conversion.getImportUrl()).openStream();
			Files.copy(in, Paths.get("files/sample.docx"), StandardCopyOption.REPLACE_EXISTING);
			
			InputStream docFile = new FileInputStream(new File("files/sample.docx"));
    		XWPFDocument doc = new XWPFDocument(docFile);
    		PdfOptions pdfOptions = PdfOptions.create();
    		OutputStream out = new FileOutputStream(new File("files/export.pdf"));
    		PdfConverter.getInstance().convert(doc, out, pdfOptions);
    		doc.close();
    		out.close();
    		
    		conversion.setExportUrl("file:///C:/Users/Murshalin/Documents/workspace-spring-tool-suite-4-4.13.0.RELEASE/file-conversion-service/file-conversion-service/files/export.pdf");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Conversion conversionOne = conversionService.save(conversion);
        return new ResponseEntity<Conversion>(conversionOne, HttpStatus.OK);
    }
}
