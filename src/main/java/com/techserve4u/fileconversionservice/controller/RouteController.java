package com.techserve4u.fileconversionservice.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;

import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;

@Controller
public class RouteController {

	@RequestMapping(value = "/excel-to-pdf", method = RequestMethod.GET)
	public String excelToPdf1() {
		
		try {
			Workbook workbook = new Workbook("files/file_example_XLS_1000.xls");
			workbook.save("files/file_example_XLS_1000.pdf", SaveFormat.PDF);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return "home";
	}
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homeRoute() {
		return "home";
	}
	
	
	@RequestMapping(value = "/word_to_pdf", method = RequestMethod.GET)
	public String wordToPdf() {
		return "word_to_pdf";
	}
	

	@RequestMapping(value = "/word_to_pdf", method = RequestMethod.POST)
	public void wordToPdf(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
	    
		try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get("files/" + file.getOriginalFilename());
            Files.write(path, bytes);
            
            InputStream docFile = new FileInputStream(new File("files/" + file.getOriginalFilename()));
    		XWPFDocument doc = new XWPFDocument(docFile);
    		PdfOptions pdfOptions = PdfOptions.create();
    		OutputStream out = new FileOutputStream(new File("files/export.pdf"));
    		PdfConverter.getInstance().convert(doc, out, pdfOptions);
    		doc.close();
    		out.close();
    		
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		// return pdf view
		try {
			File dFile = new File("files/export.pdf");
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + dFile.getName() + "\""));
			response.setContentLength((int) dFile.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(dFile));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	
	@RequestMapping(value = "/excel_to_pdf", method = RequestMethod.GET)
	public String excelToPdf() {
		return "excel_to_pdf";
	}
	
	@RequestMapping(value = "/excel_to_pdf", method = RequestMethod.POST)
	public void excelToPdf(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
	    
		try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get("files/" + file.getOriginalFilename());
            Files.write(path, bytes);
            
            Workbook workbook = new Workbook("files/"+ file.getOriginalFilename());
			workbook.save("files/export2.pdf", SaveFormat.PDF);
    		
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		// return pdf view
		try {
			File dFile = new File("files/export2.pdf");
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + dFile.getName() + "\""));
			response.setContentLength((int) dFile.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(dFile));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	@RequestMapping(value = "/image_to_pdf", method = RequestMethod.GET)
	public String imageToPdf() {
		return "image_to_pdf";
	}
	
	@RequestMapping(value = "/image_to_pdf", method = RequestMethod.POST)
	public void jpgToPdfPost(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws DocumentException, MalformedURLException, IOException {
		
		try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get("files/" + file.getOriginalFilename());
            Files.write(path, bytes);
            
            Document document = new Document(PageSize.A2);
	        document.addAuthor("morsalin");
	        document.addTitle("png to pdf");
	        PdfWriter.getInstance(document, new FileOutputStream("files/export3.pdf"));
	        document.open();
	        Image image = Image.getInstance("files/"+ file.getOriginalFilename());
	        document.add(image);
	        document.close();
	        
        } catch (Exception e) {
            e.printStackTrace();
        }
        
		try {
			File dFile = new File("files/export3.pdf");
			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			response.setContentType(mimeType);
			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + dFile.getName() + "\""));
			response.setContentLength((int) dFile.length());
			InputStream inputStream = new BufferedInputStream(new FileInputStream(dFile));
			FileCopyUtils.copy(inputStream, response.getOutputStream());
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	@RequestMapping(value = "/pptx_to_pdf", method = RequestMethod.GET)
	public String pptxToPdf() {
		return "pptx_to_pdf";
	}
	
	@RequestMapping(value = "/pptx_to_pdf", method = RequestMethod.POST)
	public String pptxToPdfPost(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws InvalidFormatException, IOException, DocumentException {
		
		try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get("files/" + file.getOriginalFilename());
            Files.write(path, bytes);
	        
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		FileInputStream inputStream = new FileInputStream("files/"+ file.getOriginalFilename());
		XMLSlideShow ppt = new XMLSlideShow(OPCPackage.open(inputStream));
		inputStream.close();
		
		Dimension pgsize = ppt.getPageSize();
	    float scale = 1;
	    int width = (int) (pgsize.width * scale);
	    int height = (int) (pgsize.height * scale);
	    
	    int i = 1;
	    int totalSlides = ppt.getSlides().size();
	    
	    for (XSLFSlide slide : ppt.getSlides()) {

            BufferedImage img = new BufferedImage(pgsize.width, pgsize.height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = img.createGraphics();
            graphics.setPaint(Color.white);
            graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width,
                    pgsize.height));
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            graphics.setColor(Color.white);
            graphics.clearRect(0, 0, width, height);
            graphics.scale(scale, scale);

            slide.draw(graphics);
            FileOutputStream out = new FileOutputStream("files/"+i+".png");
            javax.imageio.ImageIO.write(img, "png", out);
            out.close();
            i++;
       }
	    
	    Document document = new Document();
	    PdfWriter.getInstance(document, new FileOutputStream("filenew.pdf"));
	    com.lowagie.text.pdf.PdfPTable table = new com.lowagie.text.pdf.PdfPTable(1);
	    
	    for(int j = 1; j<=totalSlides; j++){
	        Image slideImage = Image.getInstance("images/"+j+".png");

	        document.setPageSize(new Rectangle(slideImage.getWidth(), slideImage.getHeight()));
	        document.open();
	        slideImage.setAbsolutePosition(0, 0);

	        table.addCell(new com.lowagie.text.pdf.PdfPCell(slideImage, true));
	    }
	    
	    document.add(table);
	    document.close();
		
		return "pptx_to_pdf";
	}
}