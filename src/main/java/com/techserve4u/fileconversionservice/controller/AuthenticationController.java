package com.techserve4u.fileconversionservice.controller;

import java.util.Random;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import com.techserve4u.fileconversionservice.entity.Auth;
import com.techserve4u.fileconversionservice.repository.AuthRepository;
import com.techserve4u.fileconversionservice.service.MailSenderService;

@Controller
public class AuthenticationController {
	
	@Autowired
	AuthRepository authRepository;
	
	@Autowired
    private MailSenderService mailSenderService;

	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String auth() {
		return "signin";
	}
	
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public String authPost(WebRequest request) {
		
		String inputEmail = request.getParameter("email");
		String inputPassword = request.getParameter("password");

		String actualPassword = authRepository.findByEmailPassword(inputEmail);

		if (actualPassword != null) {
			if (actualPassword.equals(inputPassword)) {
				return "dashboard1212";
			}
		}
		
		return "signin";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup() {
		return "signup";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(WebRequest request) {
		
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String email = request.getParameter("email").trim();
		String password = request.getParameter("password").trim();
		
		String checkUserEmail = authRepository.findByEmail(email).getEmail();
		if (checkUserEmail != null)
			return "signup";
		
		if (password.length() < 8)
			return "signup";

		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		if (password.matches(pattern) == false)
			return "signup";

		// create new user
		Auth newSignup = new Auth();
		newSignup.setName(name);
		newSignup.setPhone(phone);
		newSignup.setEmail(email);
		newSignup.setPassword(password);
		
		authRepository.save(newSignup);
		
		return "signin";
	}
	
	
	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public String forgotPassword() {
		return "forgot-password";
	}
	
	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
	public String forgotPasswordPost(WebRequest request, HttpServletResponse response) {
		
		String userEmail = request.getParameter("email");
		
		if (authRepository.findByEmail(userEmail) != null) {
			
			// make OTP
			Random rand = new Random();
			String otpCode = Integer.toString(rand.nextInt(999999));

			// update OTP to DB
			authRepository.updateOtp(otpCode, userEmail);

			// sent OTP to Email
			mailSenderService.sendPasswordOtpMail(userEmail, otpCode);
	         
	         Cookie cookie = new Cookie("resetEmail", userEmail);
			 cookie.setMaxAge(3600);
			 response.addCookie(cookie);
			 
			 return "forgot-password-otp-verify";
		}
		
		return "forgot-password";
	}
	
	@RequestMapping(value = "/forgot-password-otp-verify", method = RequestMethod.POST)
	public String forgotPasswordOtpVerify(WebRequest request, HttpServletResponse response, @CookieValue(value = "resetEmail", defaultValue = "null") String resetEmail) {
		
		String inputOtp = request.getParameter("otp").trim();
		String inputPassword = request.getParameter("password");
		
		String dbEmail = authRepository.findByEmail(resetEmail).getEmail();
		String dbOtp = authRepository.findByEmail(resetEmail).getOtp();
		
		if (resetEmail.equalsIgnoreCase(dbEmail) && inputOtp.equals(dbOtp)) {
			authRepository.updatePasswordByOtp(inputPassword, inputOtp, resetEmail);
			authRepository.updateOtp(null, resetEmail);
			return "redirect:/signin";
		}
				
		return "redirect:/forgot-password";
	}
}
