package com.techserve4u.fileconversionservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techserve4u.fileconversionservice.entity.Conversion;
import com.techserve4u.fileconversionservice.repository.ConversionRepository;

@Service
public class ConversionServiceImpl implements ConversionService {
	
	@Autowired
	ConversionRepository conversionRepository;

	@Override
	public Conversion save(Conversion conversion) {
		conversionRepository.save(conversion);
		return conversion;
	}
}
