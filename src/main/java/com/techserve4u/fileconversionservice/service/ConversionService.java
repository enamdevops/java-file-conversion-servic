package com.techserve4u.fileconversionservice.service;

import com.techserve4u.fileconversionservice.entity.Conversion;

public interface ConversionService {

	Conversion save(Conversion conversion);
}
