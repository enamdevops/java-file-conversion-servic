package com.techserve4u.fileconversionservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techserve4u.fileconversionservice.entity.Auth;
import com.techserve4u.fileconversionservice.repository.AuthRepository;

@Service
public class AuthServiceImpl implements AuthService {
	
	@Autowired
	AuthRepository authRepository;

	@Override
	public Auth signup(Auth auth) {
		authRepository.save(auth);
		return auth;
	}
}
