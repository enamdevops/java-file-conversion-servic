package com.techserve4u.fileconversionservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailSenderService {
	
	@Autowired
    private JavaMailSender javaMailSender;

	@Async
    public void sendPasswordOtpMail(String userEmail, String otpCode) {
		SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(userEmail);
        msg.setFrom("FCS Tools <laravel.trial.smtp@gmail.com>");
        msg.setSubject("Password Reset OTP File Conversion Tools");
        msg.setText("Your OTP is "+otpCode+" to reset your password.\nFor security, please do not share this with anyone.");
        javaMailSender.send(msg);
    }
}
