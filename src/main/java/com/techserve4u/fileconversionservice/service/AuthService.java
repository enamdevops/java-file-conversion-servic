package com.techserve4u.fileconversionservice.service;

import com.techserve4u.fileconversionservice.entity.Auth;

public interface AuthService {

	Auth signup(Auth auth);
}
